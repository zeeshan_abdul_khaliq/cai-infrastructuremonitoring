import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {NetworkMonService} from '../../../core-services/networkMon.service';

@Component({
  selector: 'app-networkMonVuln',
  templateUrl: './networkMonVuln.component.html',
  styleUrls: ['./networkMonVuln.component.scss'],
})
export class NetworkMonVulnComponent implements OnInit, AfterViewInit {
  requestIncident: any;
  constructor(private networkMonService: NetworkMonService, private toastr: ToastrService,
              private spinner: NgxSpinnerService, private router: Router) { }

  ngOnInit() {
  }
  ngAfterViewInit() {
    this.spinner.show('vulns-spinner');
    this.networkMonService.getNetworkMonVulnerabilties().subscribe(
        res => {
          this.spinner.hide('vulns-spinner');
          this.requestIncident = res;
        }, err => {
          this.spinner.hide('vulns-spinner');
          this.toastr.error('Some Error Occurred. Please try again');
        }
    );
  }
  ipDetail(ip) {
    this.router.navigate(['networkMon/alert-detail'], {queryParams: {id: ip, no: 2}});
  }
}
