import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {EChartsOption} from 'echarts';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {NetworkMonService} from '../../core-services/networkMon.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  response: any;
  showTable: any = true;
  charts: any = [];
  public chartOption: EChartsOption = {};
  constructor(private networkMonService: NetworkMonService, private router: Router, private toastr: ToastrService,
              private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.charts = [];
    this.spinner.show('trails-spinner');
    this.networkMonService.getNetworkMonDashboardVisualization().subscribe(
        res => {
          this.spinner.hide('trails-spinner');
          this.response = res;
          if (this.response) {
            this.showTable = true;
            for (const obj of this.response) {
              const children = [];
              for (const child of obj.stats) {
                const val =  {
                  value: child.count,
                  name: child.value,
                  id: child.id,
                };
                children.push(val);
              }
              this.chartOption = {};
              this.chartOption = {
                tooltip: {
                  trigger: 'item',
                  formatter(info: any) {
                    const value = info.value;
                    const name = info.name;
                    return  'Indicator: ' + name + '<br/>' + 'Open Ports: ' + value;

                  }
                },
                series: [
                  {

                    type: 'treemap',
                    visibleMin: 300,
                    label: {
                      show: true,
                      formatter: '{b}'
                    },
                    upperLabel: {
                      show: true,
                      height: 30
                    },
                    itemStyle: {
                      borderColor: '#fff'
                    },
                    selectedMode: false,
                    roam: false,                         // true, false, 'scale' or 'zoom', 'move'
                    nodeClick: 'zoomToNode',            // 'zoomToNode', 'link', false
                    animation: false,
                    data: children,
                    breadcrumb: {
                      show: false,
                      emptyItemWidth: 0
                    }
                  }
                ]
              };

              const data = {
                value: this.chartOption,
                name: obj.alert,
              };
              this.charts.push(data);
            }

          } else {
            this.showTable = false;

          }


        }, err => {
          this.spinner.hide('trails-spinner');
          this.showTable = false;
          this.toastr.error('Some Error Occurred. Please try again');

        }
    );
  }
  open(event) {
    this.router.navigate(['networkMon/ip-detail'], {queryParams: {id: event.data.id, no: 3}});

  }
  ngOnDestroy() {
  }
}
