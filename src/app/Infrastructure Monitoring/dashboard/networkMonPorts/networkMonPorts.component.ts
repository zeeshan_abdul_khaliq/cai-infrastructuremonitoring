import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {NetworkMonService} from '../../../core-services/networkMon.service';

@Component({
  selector: 'app-networkMonPorts',
  templateUrl: './networkMonPorts.component.html',
  styleUrls: ['./networkMonPorts.component.scss'],
})
export class NetworkMonPortsComponent implements OnInit, AfterViewInit {
  requestIncident: any;
 constructor(private networkMonService: NetworkMonService, private toastr: ToastrService,
             private spinner: NgxSpinnerService, private router: Router) { }

  ngOnInit() {
  }
  ngAfterViewInit() {
   this.spinner.show('ports-spinner');
   this.networkMonService.getNetworkMonPorts().subscribe(
      res => {
        this.spinner.hide('ports-spinner');
        this.requestIncident = res;
      }, err => {
          this.spinner.hide('ports-spinner');
          this.toastr.error('Some Error Occurred. Please try again');
      }
    );
  }
  ipDetail(ip) {
    this.router.navigate(['networkMon/alert-detail'], {queryParams: {id: ip, no: 1}});
  }
}
