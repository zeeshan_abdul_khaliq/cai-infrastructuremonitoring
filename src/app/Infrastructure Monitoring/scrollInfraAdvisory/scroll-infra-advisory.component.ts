import {Component, OnDestroy, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {DialogService, DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {CommunityTagsComponent} from '../community-tags/community-tags.component';
import {NetworkMonService} from '../../core-services/networkMon.service';

@Component({
    selector: 'app-scroll-infra-advisory',
    templateUrl: './scroll-infra-advisory.component.html',
    styleUrls: ['./scroll-infra-advisory.component.scss'],
    providers: [DialogService, DynamicDialogRef]
})
export class ScrollInfraAdvisoryComponent implements OnInit, OnDestroy {

    indicatorData: any;
    id: any;
    details: any;
    exportReporting: any = false;
    reportType: any;
    downloadType: any;
    show = false;
    tags: any = [];
    userRole: any;
    routing: boolean;
    ref: DynamicDialogRef;
    constructor(private communityService: NetworkMonService, private toastr: ToastrService, private router: Router,
                private spinner: NgxSpinnerService, private route: ActivatedRoute, public dialogService: DialogService,
                private config: DynamicDialogConfig) {
        this.route.queryParams.subscribe(params => {
            if (params.value) {
                this.id = params.value;
            }
            this.getData();
        });
        this.userRole = sessionStorage.getItem('role');
    }

    ngOnInit(): void {
    }

    expireIT(id): void  {
        this.spinner.show('comadvidialog-spinner');
        this.communityService.communityAdvisoryExpire(id).subscribe(
            res => {
                this.spinner.hide('comadvidialog-spinner');
                this.toastr.success('Advisory has been expired successfully');
                this.getData();
            },
            err => {
                this.spinner.hide('comadvidialog-spinner');
                this.toastr.error('Unable to Expire this Advisory Kindly try again');
            });
    }

    deleteIT(id): void  {
        this.spinner.show('comadvidialog-spinner');
        this.communityService.communityAdvisotyDelete(id).subscribe(
            res => {
                this.spinner.hide('comadvidialog-spinner');
                this.toastr.success('Advisory Deleted successfully');
            },
            err => {
                this.spinner.hide('comadvidialog-spinner');
                this.toastr.error('Unable to Delete Advisory Kindly try again');
            }
        );
    }

    edit(id): void  {
        this.router.navigate(['/community/updateAdvisory'], {queryParams: {id}});
    }

    getData(): void  {
        this.spinner.show('comadvidialog-spinner');
        this.tags = [];
        this.communityService.updateAdvisoryGetAll(this.id).subscribe(
            res => {
                this.spinner.hide('comadvidialog-spinner');
                this.indicatorData = res;
                this.show = true;
                this.details = this.indicatorData.indicatorDetails;
                if (this.indicatorData && this.indicatorData.length !== 0) {
                    for (let advisoryTagIndex = 0; advisoryTagIndex < this.indicatorData.advisoryTags.length;
                         advisoryTagIndex++) {
                        const data = {
                            name: this.indicatorData.advisoryTags[advisoryTagIndex].tag.name
                        };
                        this.tags.push(data);
                    }
                }
            }, err => {
                this.show = false;
                this.spinner.hide('comadvidialog-spinner');
                const error = err;
                if (error === 400) {
                    this.toastr.error(error.error.message);
                } else {
                    this.toastr.error('Oops Problem Occurred. Internal Server Error');
                }
            }
        );
    }

    openDialog(value): void  {
        this.ref = this.dialogService.open(CommunityTagsComponent, {
            header: 'Tag Appearance',
            width: '80%',
            contentStyle: {'min-height': '400px', overflow: 'auto', background: 'white'},
            baseZIndex: 10000,
            data: {type: value}
        });
    }

    ngOnDestroy(): void {
        if (this.routing) {
        this.router.navigate(['/community/advisory']);
    }
        if (this.ref) {
            this.ref.close();
        }
    }
}

