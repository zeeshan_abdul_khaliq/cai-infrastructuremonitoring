import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Route, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {NetworkMonService} from '../../core-services/networkMon.service';
@Component({
  selector: 'app-reScan',
  templateUrl: './reScan.component.html',
  styleUrls: ['./reScan.component.scss'],
})
export class ReScanComponent implements OnInit, AfterViewInit {
  requestIncident: any;
  constructor(private networkMonService: NetworkMonService, private toastrService: ToastrService,
              private spinner: NgxSpinnerService,  private router: Router) { }

  ngOnInit(): void  {
  }
  ngAfterViewInit(): void  {
   this.managerescan();
  }
  managerescan(): void  {
    this.spinner.show('rescan-spinner');
    this.networkMonService.getNetworkMonRescanList().subscribe(
      res => {
        this.spinner.hide('rescan-spinner');
        this.requestIncident = res;
      }, err => {
          this.spinner.hide('rescan-spinner');
          this.toastrService.error('Some Error Occurred. Please try again');

      }
    );
  }
  ipDetail(ip): void  {
    this.router.navigate(['networkMon/ip-detail'], {queryParams: {id: ip, no: 3}});
  }
}
