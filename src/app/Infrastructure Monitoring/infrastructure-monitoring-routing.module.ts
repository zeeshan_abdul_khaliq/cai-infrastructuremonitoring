import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {InfrastructureMonitoringComponent} from './infrastructure-monitoring.component';
import {ReScanComponent} from './reScan/reScan.component';
import {ManageAlertsComponent} from './manageAlerts/manageAlerts.component';
import {NetworkAlertDetailsComponent} from './network-alert-details/network-alert-details.component';
import {NetworkIpDetailsComponent} from './network-ip-details/network-ip-details.component';
import {ScrollInfraAdvisoryComponent} from './scrollInfraAdvisory/scroll-infra-advisory.component';

const routes: Routes = [
  {
    path: '',
    component: InfrastructureMonitoringComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'rescan',
        component: ReScanComponent
      },
      {
        path: 'manageAlerts',
        component: ManageAlertsComponent
      },
      {
        path: 'alert-detail',
        component: NetworkAlertDetailsComponent,
      },
      {
        path: 'ip-detail',
        component: NetworkIpDetailsComponent,
      },
      {
        path: 'view-infra-advisory-scroll',
        component: ScrollInfraAdvisoryComponent
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfrastructureMonitoringRoutingModule { }
