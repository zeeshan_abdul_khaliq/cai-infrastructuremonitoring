import {Component, OnDestroy, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {NetworkMonService} from '../../core-services/networkMon.service';

@Component({
    selector: 'app-community-tags',
    templateUrl: './community-tags.component.html',
    styleUrls: ['./community-tags.component.scss'],

})
export class CommunityTagsComponent implements OnInit, OnDestroy {
    indicatorData: any;
    tagName: any;
    tagTotalCount: any;
    constructor(private toastr: ToastrService, private router: Router,
                private ref: DynamicDialogRef,
                private spinner: NgxSpinnerService, private route: ActivatedRoute,
                private config: DynamicDialogConfig, private communityService: NetworkMonService) {
        if (!this.tagName) {
            this.tagName = config.data.type;
        }
    }

    ngOnInit(): void {
        this.getData();
    }

    routeToComp(type) {
        this.ref.close();
        if (type === 'Advisories') {
            this.router.navigate(['/community/apptagbaseAdvisory'], {queryParams: {tag: this.tagName}});
        } else if (type === 'Collections') {
            this.router.navigate(['/community/explore'], {queryParams: {type: 'Collections', tag: this.tagName}});
        }
    }

    getData() {
        this.spinner.show('tags-spinner');
        this.communityService.communitygetTAGdetails(this.tagName).subscribe(
            res => {
                this.spinner.hide('tags-spinner');
                const indicatorData: any = res;
                this.indicatorData = indicatorData.tagCounts;
                this.tagTotalCount = indicatorData.totalCount;
            }, err => {
                this.spinner.hide('tags-spinner');
                const error = err;
                if (error === 400) {
                    this.toastr.error(error.error.message);
                } else  {
                    this.toastr.error('Oops Problem Occurred. Internal Server Error');
                }
            }
        );
    }
    ngOnDestroy() {
        this.ref.close();
    }
}
