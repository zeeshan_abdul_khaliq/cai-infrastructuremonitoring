import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Route, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {ConfirmationService} from 'primeng/api';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {NetworkMonService} from '../../core-services/networkMon.service';
import {CreateAlertComponent} from '../createAlert/createAlert.component';
import {UpdateAlertComponent} from '../updateAlert/updateAlert.component';
@Component({
  selector: 'app-manageAlerts',
  templateUrl: './manageAlerts.component.html',
  styleUrls: ['./manageAlerts.component.scss'],
    providers: [ConfirmationService, DialogService]
})
export class ManageAlertsComponent implements OnInit, AfterViewInit {
  requestIncident: any;
  ref: DynamicDialogRef;
  constructor(private networkMonService: NetworkMonService, private toastrService: ToastrService,
              private confirmationService: ConfirmationService, public dialogService: DialogService,
              private spinner: NgxSpinnerService,  private router: Router) { }

  ngOnInit() {
  }
  ngAfterViewInit() {
   this.manageAlerts();
  }
    manageAlerts() {
    this.spinner.show('manageAlerts-spinner');
    this.networkMonService.getNetworkMonAlertList().subscribe(
      res => {
        this.spinner.hide('manageAlerts-spinner');
        this.requestIncident = res;
      }, err => {
          this.spinner.hide('manageAlerts-spinner');
          this.toastrService.error('Some Error Occurred. Please try again');

      }
    );
  }
  deleteAlert(alertid) {
        this.confirmationService.confirm({
            message: 'Are you sure you want to delete this Alert?',
            header: 'Confirmation',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.spinner.show('manageAlerts-spinner');
                this.networkMonService.networkMonDeleteAlert(alertid).subscribe(
                    res => {
                        this.toastrService.success ('Alert Deleted successfully');
                        this.spinner.hide('manageAlerts-spinner');
                        this.manageAlerts();
                    },
                    err => {
                        this.toastrService.error('Some Error Occurred. Please try again');
                        this.spinner.hide('manageAlerts-spinner');
                    }
                );
            },
            reject: () => {
            }
        });

    }
    showIPs(alertId, name) {
        this.router.navigate(['networkMon/alert-detail'], {queryParams: {id: alertId, alert: name,  no: 3}});
    }
    createAlert() {
        this.ref = this.dialogService.open(CreateAlertComponent, {
            header: 'Create Alert',
            width: '70%',
            contentStyle: {'max-height': '500px', overflow: 'auto'},
            baseZIndex: 10000
        });
        this.ref.onClose.subscribe((response: any) => {
            if (response) {
                this.manageAlerts();
            }
        });
    }
    updateAlert(alert) {
        this.ref = this.dialogService.open(UpdateAlertComponent, {
            header: 'Update Alert',
            data: alert,
            width: '70%',
            contentStyle: {'max-height': '500px', overflow: 'auto'},
            baseZIndex: 10000
        });
        this.ref.onClose.subscribe((response: any) => {
            if (response) {
                this.manageAlerts();
            }
        });
    }

}
