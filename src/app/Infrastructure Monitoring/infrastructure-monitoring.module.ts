import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TableModule} from 'primeng/table';
import {CardModule} from 'primeng/card';
import {NgxSpinnerModule} from 'ngx-spinner';
import {TooltipModule} from 'primeng/tooltip';
import {TabViewModule} from 'primeng/tabview';
import {NgxEchartsModule} from 'ngx-echarts';
import * as echarts from 'echarts';
import {CalendarModule} from 'primeng/calendar';
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MultiSelectModule} from 'primeng/multiselect';
import {AvatarModule} from 'primeng/avatar';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {DashboardComponent} from './dashboard/dashboard.component';
import {TagModule} from 'primeng/tag';
import {EditorModule} from 'primeng/editor';
import {ChipsModule} from 'primeng/chips';
import {InfrastructureMonitoringRoutingModule} from './infrastructure-monitoring-routing.module';
import {InfrastructureMonitoringComponent} from './infrastructure-monitoring.component';
import {NetworkMonPortsComponent} from './dashboard/networkMonPorts/networkMonPorts.component';
import {NetworkMonVulnComponent} from './dashboard/networkMonVuln/networkMonVuln.component';
import {ReScanComponent} from './reScan/reScan.component';
import {ManageAlertsComponent} from './manageAlerts/manageAlerts.component';
import {CreateAlertComponent} from './createAlert/createAlert.component';
import {UpdateAlertComponent} from './updateAlert/updateAlert.component';
import {NetworkIpDetailsComponent} from './network-ip-details/network-ip-details.component';
import {NetworkAlertDetailsComponent} from './network-alert-details/network-alert-details.component';
import {GoogleMapsModule} from '@angular/google-maps';
import {ScrollInfraAdvisoryComponent} from './scrollInfraAdvisory/scroll-infra-advisory.component';
import {AuthGuard} from '../core-services/auth.guard';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {TokenInterceptor} from '../core-services/token.interceptor.service';
import {ToastrModule, ToastrService} from 'ngx-toastr';
import {CommunityTagsComponent} from './community-tags/community-tags.component';
import {CoreServicesModule} from '../core-services/core-services.module';
import {ComponentsModule} from '../framework/components/components.module';

@NgModule({
    declarations: [DashboardComponent, InfrastructureMonitoringComponent, NetworkMonPortsComponent,
        CreateAlertComponent, UpdateAlertComponent, ScrollInfraAdvisoryComponent, CommunityTagsComponent,
        NetworkMonVulnComponent, ReScanComponent, ManageAlertsComponent, NetworkIpDetailsComponent,
        NetworkAlertDetailsComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        InfrastructureMonitoringRoutingModule,
        NgxSpinnerModule,
        TooltipModule,
        TabViewModule,
        TableModule,
        CardModule,
        CalendarModule,
        AvatarModule,
        EditorModule,
        ChipsModule,
        DynamicDialogModule,
        ConfirmDialogModule,
        TagModule,
        MultiSelectModule,
        GoogleMapsModule,
        CoreServicesModule,
        ComponentsModule,
        NgxDaterangepickerMd.forRoot(),
        NgxEchartsModule.forRoot({
            echarts,
        }),
        ToastrModule
    ], providers: [AuthGuard, {
        provide: HTTP_INTERCEPTORS,
        useClass: TokenInterceptor,
        multi: true,
    },
        ToastrService],
})export class InfrastructureMonitoringModule {
}
