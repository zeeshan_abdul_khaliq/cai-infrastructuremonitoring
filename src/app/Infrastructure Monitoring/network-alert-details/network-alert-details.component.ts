import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {NetworkMonService} from '../../core-services/networkMon.service';

@Component({
    selector: 'app-network-alert-details',
    templateUrl: './network-alert-details.component.html',
    styleUrls: ['./network-alert-details.component.scss']
})
export class NetworkAlertDetailsComponent implements OnInit, AfterViewInit {

    requestIncident: any;
    showTable: any;
    spin: any = true;
    alertId: any;
    no: any;
    alert: any;
    alertName: any;

    constructor(private networkMonService: NetworkMonService, private router: Router,
                private toastrService: ToastrService,
                public spinnerService: NgxSpinnerService,
                private route: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.spinnerService.show('createAlert-spinner');
        this.alertId = this.route.snapshot.queryParamMap.get('id');
        this.no = this.route.snapshot.queryParamMap.get('no');
        this.alertName = this.route.snapshot.queryParamMap.get('alert');
    }

    ngAfterViewInit(): void  {
        this.networkMonService.getNetworkMonIpList(this.alertId, this.no).subscribe(
            res => {
                this.spinnerService.hide('createAlert-spinner');
                this.requestIncident = res;
                if (this.requestIncident) {
                    this.showTable = true;
                } else {
                    this.showTable = false;
                }


            }, err => {
                this.spinnerService.hide('createAlert-spinner');
                this.showTable = false;
                this.toastrService.error('500 Internal Server Error..');
            }
        );
    }

    ipDetail(ip): void  {
        this.router.navigate(['networkMon/ip-detail'], {queryParams: {id: ip, no: 3}});
    }
}
