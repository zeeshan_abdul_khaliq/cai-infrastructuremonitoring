import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkAlertDetailsComponent } from './network-alert-details.component';

describe('NetworkAlertDetailsComponent', () => {
  let component: NetworkAlertDetailsComponent;
  let fixture: ComponentFixture<NetworkAlertDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NetworkAlertDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkAlertDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
