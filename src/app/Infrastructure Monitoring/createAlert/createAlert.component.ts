import {Component, OnInit, ViewChild} from '@angular/core';
import {Route, Router} from '@angular/router';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NetworkMonService} from '../../core-services/networkMon.service';

@Component({
  selector: 'app-createAlert',
  templateUrl: './createAlert.component.html',
  styleUrls: ['./createAlert.component.scss'],
})
export class CreateAlertComponent implements OnInit {
  showTable: any;
  createAlert: FormGroup;
  ipTags: any[];
  fortag: any;
  tagsrequired = false;
  respose: any;
  checkboxes = [
    {
      name: 'Industrial Control System',
      description: 'Services associated with industrial control systems',
      value: 'industrialControlSystem',
      selected: false
    },
    {
      name: 'Internet Scanner',
      description: ' Device has been seen scanning the Internet and exposes a service',
      value: 'internetScanner',
      selected: false
    },
    {name: 'IoT', description: 'Service associated with Internet of Things Devices', value: 'iot', selected: false},
    {name: 'Malware', description: 'Compromised or malware-related services', value: 'malware', selected: false},
    {name: 'New Service', description: 'New open port/ service discovered', value: 'newService', selected: false},
    {name: 'Open Database', description: 'Database service that does not require authentication', value: 'openDatabase', selected: false},
    {name: 'SSL Expired', description: 'Expired SSL certificate is used by the service', value: 'sslExpired', selected: false},
    {name: 'Uncommon', description: 'Services that generally shouldn’t be publicly available', value: 'uncommon', selected: false},
    {name: 'Vulnerable', description: 'Service is vulnerable to a known issue', value: 'vulnerable', selected: false},
  ];

  // }
  constructor(private networkMonService: NetworkMonService, private toastrService: ToastrService, private router: Router,
              public spinnerService: NgxSpinnerService,  public ref: DynamicDialogRef, private formBuilder: FormBuilder,
              public config: DynamicDialogConfig) {
    const controlArray = this.checkboxes.map(c => new FormControl(c.selected || false));
    this.createAlert = this.formBuilder.group({
      main: ['', [Validators.required, Validators.pattern( '^[ A-Za-z0-9_.%,@#&$:_-]+$')]],
      tags: ['', [Validators.required]],
      notifications:  new FormArray(controlArray)
    });

  }
  get errorControl() {
    return this.createAlert.controls;
  }

  ngOnInit(): void {
    this.ipTags = [];
  }
  cancel(): void {
    this.ref.close();
  }

  add(event): void {
    const eventInput = event.input;
    const eventValue = event.value;
    if ((eventValue || '').trim()) {
      const pattern = new RegExp(/^(?!0.0.0.0)(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(?:\/(\d|[12]\d|3[0-2]))?$/);
      if (!pattern.test(eventValue)) {
        const length = this.createAlert.value.tags.length;
        this.createAlert.value.tags.splice(length - 1, 1);
      } else {
        if (this.ipTags.length > 0) {
          if (this.ipTags.find((test) => test.name.toLowerCase() === eventValue.toLowerCase())) {
          } else {
            this.ipTags.push({name: eventValue.trim()});
          }
        } else {
          this.ipTags.push({name: eventValue.trim()});

        }
      }
    }
    // Reset the input eventValue
    if (eventInput) {
      eventInput.value = '';
    }
  }
  remove(event: any): void {
    const index = this.ipTags.findIndex(x => x.name === event.value);
    if (index >= 0) {
      this.ipTags.splice(index, 1);
    }
  }
  onSubmit() {
    if (this.createAlert.invalid) {
      this.toastrService.error('Please fill the form');
      return;
    }
    const event = this.createAlert.value;
    this.tagsrequired = false;
    this.fortag = [];
    if (this.ipTags && this.ipTags.length > 0) {
      this.tagsrequired = false;
      for (let i = 0; i < this.ipTags.length; i++) {
        this.fortag.push(this.ipTags[i].name);
      }
    }
    const data = {
        name: event.main,
        filters: {
          ip: this.fortag
        },
        triggers: {
          industrialControlSystem: event.notifications[0],
          internetScanner: event.notifications[1],
          iot: event.notifications[2],
          malware: event.notifications[3],
          newService: event.notifications[4],
          openDatabase: event.notifications[5],
          sslExpired: event.notifications[6],
          uncommon: event.notifications[7],
          vulnerable: event.notifications[8]
        }
      };
    this.spinnerService.show('createAlert-spinner');
    this.networkMonService.networkMoncreateAlert(data).subscribe(
          res => {
            this.spinnerService.hide('createAlert-spinner');
            this.respose = res;
            if (this.respose && this.respose.responseDto && this.respose.responseDto.responseCode === Number(422)) {
              this.toastrService.error(this.respose.responseDto.response);
            } else {
              this.ipTags = [];
              this.toastrService.success('Alert Created Successfully');
              this.ref.close(this.respose.responseDto);
            }
          },
          error => {
            this.spinnerService.hide('createAlert-spinner');
            const err = error.error.responseDto;
            if (error.error.status === Number(500)) {
              this.toastrService.error('Some Error Occurred. Please try again');
            } else {
              if (err.response) {
                this.toastrService.error(err.response);
              }
            }
          });
  }


}
