import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkIpDetailsComponent } from './network-ip-details.component';

describe('NetworkIpDetailsComponent', () => {
  let component: NetworkIpDetailsComponent;
  let fixture: ComponentFixture<NetworkIpDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NetworkIpDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkIpDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
