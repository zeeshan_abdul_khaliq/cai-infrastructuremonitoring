import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {NetworkMonService} from '../../core-services/networkMon.service';

@Component({
  selector: 'app-network-ip-details',
  templateUrl: './network-ip-details.component.html',
  styleUrls: ['./network-ip-details.component.scss']
})
export class NetworkIpDetailsComponent implements OnInit, AfterViewInit {

  details: any;
  showTable: any;
  spin: any = true;
  alertId: any;
  portData: any;
  ssl: any;
  ssh: any;
  expire: any;
  ports: any = [];
  vuln: any = [];
  data: any = [];
  vulnData: any;
  lat = 51.678418;
  lng = 7.809007;
  center: google.maps.LatLngLiteral;
  options: google.maps.MapOptions = {
    mapTypeId: 'terrain',
    zoomControl: false,
    scrollwheel: false,
    disableDoubleClickZoom: true,
    disableDefaultUI: true,

    maxZoom: 15,
    minZoom: 8,
  };
  zoom = 15;
  constructor(private networkMonService: NetworkMonService, private router: Router, private toastrService: ToastrService,
              public spinnerService: NgxSpinnerService, private route: ActivatedRoute) { }

  zoomIn() {
    if (this.zoom < this.options.maxZoom) { this.zoom++; }
  }

  zoomOut(): void  {
    if (this.zoom > this.options.minZoom) { this.zoom--; }
  }

  ngOnInit(): void  {
    navigator.geolocation.getCurrentPosition((position) => {
      this.center = {
        lat: this.lat,
        lng: this.lng,
      };
    });
    this.alertId = this.route.snapshot.queryParamMap.get('id');
  }
  ngAfterViewInit(): void  {
    this.spinnerService.show('createInfa-spinner');
    this.networkMonService.getNetworkMonIpDetail(this.alertId, false).subscribe(
        res => {
          this.spinnerService.hide('createInfa-spinner');
          this.details = res;
          if (this.details  && this.details.response) {
            this.details = this.details.response;
            this.details.date = this.details.last_update.split('+');
            console.log(this.details);
            this.ports = [];
            if (this.details && this.details.ports  && this.details.ports.length > 0) {
              this.ports = this.details.ports.sort((a, b) => a - b);
              this.details.ports = this.ports;
              for (let i = 0; i < this.details.data.length; i++) {
                if (this.details.data[i].ssl && this.details.data[i].ssl.cert && this.details.data[i].ssl.cert.expires) {
                  const year = this.details.data[i].ssl.cert.expires.substr(0, 4);
                  const mon = this.details.data[i].ssl.cert.expires.substr(4, 2);
                  const dat = this.details.data[i].ssl.cert.expires.substr(6, 2);
                  this.details.data[i].ssl.cert.expires = dat + '-' + mon + '-' + year;
                }
                if (this.details.data[i].ssl && this.details.data[i].ssl.cert && this.details.data[i].ssl.cert.issued) {
                  const year = this.details.data[i].ssl.cert.issued.substr(0, 4);
                  const mon = this.details.data[i].ssl.cert.issued.substr(4, 2);
                  const dat = this.details.data[i].ssl.cert.issued.substr(6, 2);
                  this.details.data[i].ssl.cert.issued = dat + '-' + mon + '-' + year;
                }
              }
              this.portData = this.details.data.find(x => x.port === this.details.ports[0]);
              this.ssl = this.portData.ssl;
              this.ssh = this.portData.ssh;
            }
            if (this.details?.latitude && this.details?.longitude) {
              navigator.geolocation.getCurrentPosition((position) => {
                this.center = {
                  lat: this.details?.latitude,
                  lng: this.details?.longitude,
                };
              });
            }
            if (this.details && this.details.vulns && this.details.vulns.length > 0) {
              this.vuln = [];
              const newArr = [];
              for ( const vul of this.details.data) {
                if (vul.vulns) {
                  console.log(vul.vulns);
                  Object.entries(vul.vulns).forEach(
                      ([key, value]) => {
                        const data = {
                          key,
                          value
                        };

                        this.vuln.push(data);
                      }
                  );
                }
              }
              this.vuln.forEach((item, index) => {
                if (newArr.findIndex(i => i.key == item.key) === -1) {
                  newArr.push(item);
                }
              });
              this.vuln = newArr;
              this.vulnData = this.vuln.find(x => x.key === this.details.vulns[0]);

            }
            this.showTable = true;
          } else {
            this.showTable = false;
          }


        }, err => {
          this.spinnerService.hide('createInfa-spinner');
          this.showTable = false;
          this.toastrService.error('500 Internal Server Error..');
        }
    );
  }
  reScan(): void  {
    this.spinnerService.show('createInfa-spinner');
    this.networkMonService.getNetworkMonIpDetail(this.alertId, true).subscribe(
        res => {
          this.spinnerService.hide('createInfa-spinner');
          this.details = res;
          this.toastrService.success(this.details.detailedMessage);
          this.router.navigate(['networkMon/rescan']);

        }, err => {
          this.spinnerService.hide('createInfa-spinner');
          this.toastrService.error('500 Internal Server Error..');
        }
    );
  }
  selectTab(event): void  {
    this.portData = this.details.data.find(x => x.port === this.details.ports[event.index]);
    this.ssl = this.portData.ssl;
    this.ssh = this.portData.ssh;

  }
  selectVulnTab(event): void  {
    this.vulnData = this.vuln.find(x => x.key === this.details.vulns[event.index]);

  }

  initMap(): void {
    const myLatLng = { lat: this.lat, lng: this.lng };

    const map = new google.maps.Map(
        document.getElementById('map') as HTMLElement,
        {
          zoom: 4,
          center: myLatLng,
        }
    );

    new google.maps.Marker({
      position: myLatLng,
      map,
      title: 'Hello World!',
    });
  }
}
