import {Component, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {MessageService} from 'primeng/api';
import {Route, Router} from '@angular/router';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {NgxSpinnerService} from 'ngx-spinner';
import {DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {NetworkMonService} from '../../core-services/networkMon.service';
@Component({
  selector: 'app-updateAlert',
  templateUrl: './updateAlert.component.html',
  styleUrls: ['./updateAlert.component.scss'],
  providers: [MessageService]
})
export class UpdateAlertComponent implements OnInit {
  showTable: any;
  createAlert: FormGroup;
  tagss: any[];
  fortag: any;
  tagsrequired = false;
  respose: any;
  alert: any;
  checkboxes = [
    {
      name: 'Industrial Control System',
      description: 'Services associated with industrial control systems',
      value: 'industrialControlSystem',
      selected: false
    },
    {
      name: 'Internet Scanner',
      description: ' Device has been seen scanning the Internet and exposes a service',
      value: 'internetScanner',
      selected: false
    },
    {name: 'IoT', description: 'Service associated with Internet of Things Devices', value: 'iot', selected: false},
    {name: 'Malware', description: 'Compromised or malware-related services', value: 'malware', selected: false},
    {name: 'New Service', description: 'New open port/ service discovered', value: 'newService', selected: false},
    {name: 'Open Database', description: 'Database service that does not require authentication', value: 'openDatabase', selected: false},
    {name: 'SSL Expired', description: 'Expired SSL certificate is used by the service', value: 'sslExpired', selected: false},
    {name: 'Uncommon', description: 'Services that generally shouldn’t be publicly available', value: 'uncommon', selected: false},
    {name: 'Vulnerable', description: 'Service is vulnerable to a known issue', value: 'vulnerable', selected: false},
  ];

  // }
  constructor(private networkMonService: NetworkMonService, private toastrService: ToastrService, private router: Router,
              public spinnerService: NgxSpinnerService,  public ref: DynamicDialogRef, private formBuilder: FormBuilder,
              public config: DynamicDialogConfig) {
    this.tagss = [];
    this.alert = this.config.data;
    if (this.alert) {
      if (this.alert.linkedIps && this.alert.linkedIps.length > 0) {
        for (const ip of this.alert.linkedIps) {
          const ips = {
            name: ip
          };
          this.tagss.push(ips);
        }
      }
      this.checkboxes[0].selected = this.alert.triggers.industrialControlSystem;
      this.checkboxes[1].selected = this.alert.triggers.internetScanner;
      this.checkboxes[2].selected = this.alert.triggers.iot;
      this.checkboxes[3].selected = this.alert.triggers.malware;
      this.checkboxes[4].selected = this.alert.triggers.newService;
      this.checkboxes[5].selected = this.alert.triggers.openDatabase;
      this.checkboxes[6].selected = this.alert.triggers.sslExpired;
      this.checkboxes[7].selected = this.alert.triggers.uncommon;
      this.checkboxes[8].selected = this.alert.triggers.vulnerable;
    }
    const controlArray = this.checkboxes.map(c => new FormControl(c.selected || false));
    this.createAlert = this.formBuilder.group({
      main: [this.alert.name],
      notifications:  new FormArray(controlArray)
    });
  }
  get errorControl(): any  {
    return this.createAlert.controls;
  }

  ngOnInit(): void  {
  }
  onNoClick(): void {
    this.ref.close();
  }
  cancel(): void  {
    this.ref.close();
  }

  add(event): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      const patt = new RegExp('^(?!0\\.0\\.0\\.0/)(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}' +
          '([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\\/\\d+)?$');
      console.log(patt.test(value));
      if (!patt.test(value)) {
      } else {
        if (this.tagss.length > 0) {
          if (this.tagss.find((test) => test.name.toLowerCase() === value.toLowerCase())) {
          } else {
            this.tagss.push({name: value.trim()});
          }
        } else {
          this.tagss.push({name: value.trim()});

        }
      }
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  onSubmit(): void {
    console.log(this.createAlert.value);
    if (this.createAlert.invalid) {
      this.toastrService.error('Please fill the form correctly');
      return;
    }
    const event = this.createAlert.value;
    this.tagsrequired = false;
    this.fortag = [];
    if (this.tagss && this.tagss.length > 0) {
      this.tagsrequired = false;
      for (let i = 0; i < this.tagss.length; i++) {
        this.fortag.push(this.tagss[i].name);
      }
    }
    const data = {
      name: event.main,
      filters: {
        ip: this.fortag
      },
      id: this.alert.sourceId,
      triggers: {
        industrialControlSystem: event.notifications[0],
        internetScanner: event.notifications[1],
        iot: event.notifications[2],
        malware: event.notifications[3],
        newService: event.notifications[4],
        openDatabase: event.notifications[5],
        sslExpired: event.notifications[6],
        uncommon: event.notifications[7],
        vulnerable: event.notifications[8]
      }
    };
    this.spinnerService.show('updateAlert-spinner');
    this.networkMonService.networkMonupdateAlert(data).subscribe(
        res => {
          this.spinnerService.hide('updateAlert-spinner');
          this.tagss = [];
          this.respose = res;
          this.toastrService.success('Alert Updated Successfully');
          this.ref.close(this.respose.responseDto);
        },
        error => {
          this.spinnerService.hide('updateAlert-spinner');
          const err = error.error.responseDto;
          if (error.error.status === Number(500)) {
            this.toastrService.error('Some Error Occurred. Please try again');
          } else {
            if (err.response) {
              this.toastrService.error(err.response);
            }
          }
        });
  }

  remove(fruit: any): void {
    const index = this.tagss.indexOf(fruit);
    if (index >= 0) {
      this.tagss.splice(index, 1);
    }
  }
}
