import { NgModule, ModuleWithProviders } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {NetworkMonService} from './networkMon.service';
import {HttpClientModule} from '@angular/common/http';
import {ConfigApiService} from './configApi.service';
import {AuthService} from './auth.service';
import {TokenInterceptor} from './token.interceptor.service';
import {ToastrModule, ToastrService} from 'ngx-toastr';
import {CalendarSettingService} from './calendarSetting.service';
const SERVICES = [
    NetworkMonService,
    ConfigApiService,
    AuthService,
    TokenInterceptor
];
@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        ToastrModule.forRoot()

    ],
    declarations:
        [],
    providers: [
        ...SERVICES,
        DatePipe,
        ToastrService,
        CalendarSettingService
    ],
})
export class CoreServicesModule {
    static forRoot(): ModuleWithProviders <any> {
        return {
            ngModule: CoreServicesModule,
            providers: [
                ...SERVICES,
            ],
        };
    }
}
