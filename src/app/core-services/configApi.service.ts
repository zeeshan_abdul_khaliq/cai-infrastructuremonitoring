import {Injectable, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';
@Injectable()
export class ConfigApiService {
    baseUrl: string;
    stixUrlFeeds: string;

    constructor() {
        this.baseUrl = environment.API_URL;
        this.stixUrlFeeds = environment.STIX_Url_Feeds;
    }

    /*-----------------------Adding to implement Netwrok MOnitoring------------------------------------------*/
    getNetworkMonPorts(): string {
        const login = this.baseUrl + 'infra/alert/open-ports';
        return login;
    }

    getNetworkMonRescanList(): string {
        const login = this.baseUrl + 'infra/rescan-list';
        return login;
    }

    getNetworkMonDashboardVisualization(): string {
        const login = this.baseUrl + 'infra/alert/ip-port-counts';
        return login;
    }

    getNetworkMonIpList(alertId): string {
        const login = this.baseUrl + 'infra/alert/ip-list?alertId=' + alertId;
        return login;
    }

    getNetworkMonCVEIpList(alertId): string {
        const login = this.baseUrl + 'infra/alert/ip-list?vulnerability=' + alertId;
        return login;
    }

    getNetworkMonPortIpList(alertId): string {
        const login = this.baseUrl + 'infra/alert/ip-list?port=' + alertId;
        return login;
    }

    getNetworkMonIpDetail(ip, scan): string {
        const login = this.baseUrl + 'infra/alert/ip-details?id=' + ip + '&rescan=' + scan;
        return login;
    }

    getNetworkMonVulnerabilties(): string {
        const login = this.baseUrl + 'infra/alert/vulnerabilities';
        return login;
    }

    getNetworkMonAlertList(): string {
        const login = this.baseUrl + 'infra/alert/list';
        return login;
    }

    networkMoncreateAlert(): string {
        const login = this.baseUrl + 'infra/alert';
        return login;
    }

    networkMonDeleteAlert(alertid): string {
        const login = this.baseUrl + 'infra/alert?id=' + alertid;
        return login;
    }
    communitygetTAGdetails(tag): string {
        return this.baseUrl + 'community/tags/tag-counts?value=' + tag;
    }
    communityAdvisotyDelete(id): string {
        return this.baseUrl + 'community/advisory?id=' + id;
    }

    communityAdvisoryExpire(id): string {
        return this.baseUrl + 'community/advisory/expire?id=' + id;
    }

    updateAdvisoryGetAll(id): string {
        return this.baseUrl + 'community/advisory?id=' + id;
    }
}
