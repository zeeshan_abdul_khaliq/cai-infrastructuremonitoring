import {Injectable, OnInit} from '@angular/core';
import {ConfigApiService} from './configApi.service';
import {HttpClient, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
@Injectable()
export class NetworkMonService {
    baseUrl: any;
    success: any;

    constructor(private http: HttpClient, private configApi: ConfigApiService) {
    }

    /*--------------------------UPDATED DRP SERVICES------------------------------------------*/
    getNetworkMonPorts(): any {
        return this.http.get(this.configApi.getNetworkMonPorts());
    }
    getNetworkMonRescanList(): any {
        return this.http.get(this.configApi.getNetworkMonRescanList());
    }
    getNetworkMonDashboardVisualization(): any {
        return this.http.get(this.configApi.getNetworkMonDashboardVisualization());
    }
    getNetworkMonIpList(alertId, no): any {
        if (no === '1') {
            return this.http.get(this.configApi.getNetworkMonPortIpList(alertId));
        } else if (no === '2') {
            return this.http.get(this.configApi.getNetworkMonCVEIpList(alertId));
        } else if (no === '3') {
            return this.http.get(this.configApi.getNetworkMonIpList(alertId));
        }
    }
    getNetworkMonIpDetail(ip, scan): any {
        return this.http.get(this.configApi.getNetworkMonIpDetail(ip, scan));
    }
    getNetworkMonVulnerabilties(): any {
        return this.http.get(this.configApi.getNetworkMonVulnerabilties());
    }
    getNetworkMonAlertList(): any {
        return this.http.get(this.configApi.getNetworkMonAlertList());
    }
    networkMoncreateAlert(body): any {
        return this.http.post(this.configApi.networkMoncreateAlert(), body);
    }
    networkMonupdateAlert(body): any {
        return this.http.put(this.configApi.networkMoncreateAlert(), body);
    }
    networkMonDeleteAlert(alertid): any {
        return this.http.delete(this.configApi.networkMonDeleteAlert(alertid));
    }
    communitygetTAGdetails(tag): any {
        const url = this.configApi.communitygetTAGdetails(tag);
        return this.http.get(url);
    }
    communityAdvisotyDelete(id): any {
        const url = this.configApi.communityAdvisotyDelete(id);
        return this.http.delete(url);
    }
    communityAdvisoryExpire(id): any {
        const url = this.configApi.communityAdvisoryExpire(id);
        return this.http.put(url, null);
    }

    updateAdvisoryGetAll(id): any {
        const url = this.configApi.updateAdvisoryGetAll(id);
        return this.http.get(url);
    }
}
