import { NgModule } from '@angular/core';
import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import {InfrastructureMonitoringModule} from './Infrastructure Monitoring/infrastructure-monitoring.module';
const routes: Routes = [];
@NgModule({
  imports: [RouterModule.forRoot(routes, {  }), InfrastructureMonitoringModule],
  exports: [RouterModule],
})
export class AppRoutingModule { }
