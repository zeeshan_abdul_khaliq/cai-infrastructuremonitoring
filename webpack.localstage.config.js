const webpack = require("webpack");
const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

module.exports = {
  output: {
    publicPath: "http://192.168.18.11:80",
    uniqueName: "networkMon",
  },
  optimization: {
    runtimeChunk: false,
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "networkMon",
      library: { type: "var", name: "networkMon" },
      filename: "remoteEntry.js",
      exposes: {
        InfrastructureMonitoringModule: "./src/app/Infrastructure Monitoring/infrastructure-monitoring.module.ts",
      },
      shared: {
        "@angular/core": { singleton: true, requiredVersion:'12.0.1'  },
        "@angular/common": { singleton: true, requiredVersion:'12.0.1'  },
        "@angular/router": { singleton: true, requiredVersion:'12.0.1'  },
      },
    }),
  ],
};
